#!/usr/bin/env bash

### Tests for ./getopt_case_tester.sh (or whatever executable you point TESTED_FP at).

## -----------------------------------------------------------------------
## messaging constants
## -----------------------------------------------------------------------

THIS_FP="${0}"
THIS_FN="$(basename ${THIS_FP})"
THIS_DIR="$(readlink -f $(dirname ${THIS_FP}))"  # FQ path to caller's $(pwd)
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

## -----------------------------------------------------------------------
## what these tests are testing
## -----------------------------------------------------------------------

TESTED_FN="${THIS_FN%_tests.sh}.sh"
TESTED_FP="${THIS_DIR}/${TESTED_FN}"

## -----------------------------------------------------------------------
## test stats
## -----------------------------------------------------------------------

(( TESTED_N = 0 ))
(( FAILED_N = 0 ))

## -----------------------------------------------------------------------
## code
## -----------------------------------------------------------------------

function compare_result_to_expected_for_options {
    local TEST_N="${1}"
    local OPTS_STR="${2}"
    local EXPECT_STR="${3}"
    local RESULT_STR="$("${TESTED_FP}" ${OPTS_STR})"

    if [[ "${RESULT_STR}" == "${EXPECT_STR}" ]] ; then
        echo "passed test#=${TEST_N} testing options='${OPTS_STR}'"
    else
        >&2 echo "FAILED test#=${TEST_N} testing options='${OPTS_STR}'"
        >&2 echo "expected response string='${EXPECT_STR}'"
        >&2 echo "received response string='${RESULT_STR}'"
        (( FAILED_N += 1 )) # increment |tests failed|
    fi
    (( TESTED_N += 1 ))     # increment |tests run|
    echo # newline
} # end function compare_result_to_expected_for_options

if   [[ -z "${TESTED_FP}" ]] ; then
    >&2 echo "${ERROR_PREFIX} TESTED_FP undefined, exiting ..."
    exit 2
elif [[ ! -x "${TESTED_FP}" ]] ; then
    >&2 echo "${ERROR_PREFIX} cannot execute TESTED_FP='${TESTED_FP}', exiting ..."
    exit 3
fi

echo "${MESSAGE_PREFIX} tests run @ $(date)"
echo # newline

## -----------------------------------------------------------------------
## tests
## -----------------------------------------------------------------------

TEST_N='01'
OPTS_STR='--help'
EXPECT_STR="${TESTED_FN}: set option vars=
HELP"
compare_result_to_expected_for_options "${TEST_N}" "${OPTS_STR}" "${EXPECT_STR}"

## -----------------------------------------------------------------------

TEST_N='02'
OPTS_STR='--foo'
EXPECT_STR="${TESTED_FN}: set option vars=
FOO"
compare_result_to_expected_for_options "${TEST_N}" "${OPTS_STR}" "${EXPECT_STR}"

## -----------------------------------------------------------------------

TEST_N='03'
OPTS_STR='--bar'
EXPECT_STR="${TESTED_FN}: set option vars=
BAR"
compare_result_to_expected_for_options "${TEST_N}" "${OPTS_STR}" "${EXPECT_STR}"

## -----------------------------------------------------------------------

TEST_N='04'
OPTS_STR='--foo --bar'
EXPECT_STR="${TESTED_FN}: set option vars=
BAR
FOO"
compare_result_to_expected_for_options "${TEST_N}" "${OPTS_STR}" "${EXPECT_STR}"

## -----------------------------------------------------------------------

TEST_N='05'
OPTS_STR='--bar --foo'
EXPECT_STR="${TESTED_FN}: set option vars=
BAR
FOO"
compare_result_to_expected_for_options "${TEST_N}" "${OPTS_STR}" "${EXPECT_STR}"

## -----------------------------------------------------------------------

TEST_N='06'
OPTS_STR='--caz'
EXPECT_STR="${TESTED_FN}: set option vars=
BAR
CAZ
FOO"
compare_result_to_expected_for_options "${TEST_N}" "${OPTS_STR}" "${EXPECT_STR}"

## -----------------------------------------------------------------------

TEST_N='07'
OPTS_STR='--help --caz'
EXPECT_STR="${TESTED_FN}: set option vars=
HELP"
compare_result_to_expected_for_options "${TEST_N}" "${OPTS_STR}" "${EXPECT_STR}"

## -----------------------------------------------------------------------

TEST_N='08'
OPTS_STR='--debug --caz'
EXPECT_STR="${TESTED_FN}: set option vars=
BAR
CAZ
DEBUG
FOO"
compare_result_to_expected_for_options "${TEST_N}" "${OPTS_STR}" "${EXPECT_STR}"

## -----------------------------------------------------------------------

TEST_N='09'
OPTS_STR='--caz --debug'
EXPECT_STR="${TESTED_FN}: set option vars=
BAR
CAZ
DEBUG
FOO"
compare_result_to_expected_for_options "${TEST_N}" "${OPTS_STR}" "${EXPECT_STR}"

## -----------------------------------------------------------------------

TEST_N='10'
OPTS_STR='--foo --bar --debug'
EXPECT_STR="${TESTED_FN}: set option vars=
BAR
DEBUG
FOO"
compare_result_to_expected_for_options "${TEST_N}" "${OPTS_STR}" "${EXPECT_STR}"

## -----------------------------------------------------------------------

TEST_N='11'
OPTS_STR='--foo --debug --bar'
EXPECT_STR="${TESTED_FN}: set option vars=
BAR
DEBUG
FOO"
compare_result_to_expected_for_options "${TEST_N}" "${OPTS_STR}" "${EXPECT_STR}"

## -----------------------------------------------------------------------

TEST_N='12'
OPTS_STR='--debug --foo --bar'
EXPECT_STR="${TESTED_FN}: set option vars=
BAR
DEBUG
FOO"
compare_result_to_expected_for_options "${TEST_N}" "${OPTS_STR}" "${EXPECT_STR}"

## -----------------------------------------------------------------------
## results/stats
## -----------------------------------------------------------------------

if (( FAILED_N == 0 )) ; then
    echo "${MESSAGE_PREFIX} passed all of ${TESTED_N} tests"
    echo # newline
    exit 0
else
    >&2 echo "${ERROR_PREFIX} failed ${FAILED_N} of ${TESTED_N} tests"
    echo # newline
    exit 1
fi

exit 0 # JIC
