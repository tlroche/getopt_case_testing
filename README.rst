+++++++++++++++++++
getopt_case_testing
+++++++++++++++++++

.. contents:: **table of contents**

purpose
+++++++

Investigating (hopefully answering :-) a problem (now fixed ``!-)`` I had making a `bash <https://en.wikipedia.org/wiki/Bash_(Unix_shell)>`_ script do what I want when driving `getopt <https://en.wikipedia.org/wiki/Getopt>`_ and a `case statement <https://en.wikipedia.org/wiki/Switch_statement>`_. That other bash script is waaay more complicated than this example; however its option-parsing requirements are almost identical to the boiled-down example presented here, which should be much more readable/understandable, which is why I took the time to create this. (That, and the hope that this will be useful to OP, and the possibility that I will add additional problems/examples to this repo in future if I get stuck on similar issues.)

description
+++++++++++

What I want to do is stated in the comment at the top of `getopt_case_tester.sh`_: I'd copy it here, but I don't want that and this (README) to get out of sync, so just follow that link. I automate testing of ``getopt_case_tester.sh`` by driving it with `getopt_case_tester_tests.sh`_. Those 2 commandline/headless scripts are the functional whole of this repo/project.

.. _getopt_case_tester.sh: ./getopt_case_tester.sh
.. _tester: ./getopt_case_tester.sh
.. _getopt_case_tester_tests.sh: ./getopt_case_tester_tests.sh
.. _tests: ./getopt_case_tester_tests.sh

usage
+++++

1. Get both scripts: either

    * ``git clone`` `this repo <https://bitbucket.org/tlroche/getopt_case_testing>`_ however you usually do that.
    * download the raw versions of the 2 scripts (`tester`_ and `tests`_)

        1. to same directory/folder.
        #. make both scripts executable

#. Run your local `getopt_case_tester_tests.sh`_ from a console commandline (preferably with fully-qualified path).
#. [Debug, edit, run] loop:

    1. Edit your local `getopt_case_tester.sh`_
    #. Run ``getopt_case_tester_tests.sh``
    #. Repeat previous 2 steps until all tests pass. Test status should be obvious from ``getopt_case_tester_tests.sh`` output: see section=status below.

#. **(optional, extra credit)** Post a `pull request <https://bitbucket.org/tlroche/getopt_case_testing/pull-requests/>`_ with working code.

status
++++++

Currently all tests pass:

::

    me@it: ~ $ ~/repos/Bitbucket/public/getopt_case_testing/getopt_case_tester_tests.sh
    getopt_case_tester_tests.sh: tests run @ Sun Oct  1 14:55:02 MST 2017

    passed test#=01 testing options='--help'

    passed test#=02 testing options='--foo'

    passed test#=03 testing options='--bar'

    passed test#=04 testing options='--foo --bar'

    passed test#=05 testing options='--bar --foo'

    passed test#=06 testing options='--caz'

    passed test#=07 testing options='--help --caz'

    passed test#=08 testing options='--debug --caz'

    passed test#=09 testing options='--caz --debug'

    passed test#=10 testing options='--foo --bar --debug'

    passed test#=11 testing options='--foo --debug --bar'

    passed test#=12 testing options='--debug --foo --bar'

    getopt_case_tester_tests.sh: passed all of 12 tests
