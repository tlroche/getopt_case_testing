#!/usr/bin/env bash

### This script seeks to parse commandline options in a particular way:

### * Each defined option must set a matching/shadowing boolean variable.
### * `--foo` must set `$FOO` (in addition to any other previously-set option).
### * `--bar` must set `$BAR` (in addition to any other previously-set option).
### * `--caz` must set `$FOO`, `$BAR`, and `$CAZ` (in addition to any other previously-set option).
### * `--debug` must set `$DEBUG` (in addition to any other previously-set option).
### * `--help` must set only `$HELP`, then cease parsing options.
### * On any option not in the above list: set only `$HELP`, then cease parsing options.

## -----------------------------------------------------------------------
## messaging constants
## -----------------------------------------------------------------------

THIS_FP="${0}"
THIS_FN="$(basename ${THIS_FP})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

## -----------------------------------------------------------------------
## behavioral constants. These are "real bash booleans", i.e.,
## * their values are executables (/bin/false || /bin/true), so not quoted
## * they can be overridden by commandline options
## -----------------------------------------------------------------------

FOO=false    # matches -f|--foo
BAR=false    # matches -b|--bar
CAZ=false    # matches -c|--caz (instead of `baz`, for shortopt purposes)
DEBUG=false  # matches -d|--debug
HELP=false   # matches -h|--help

## -----------------------------------------------------------------------
## code
## -----------------------------------------------------------------------

## -----------------------------------------------------------------------
## parse commandline options
## -----------------------------------------------------------------------

SHORTOPTS='bcdfh'
LONGOPTS='bar,caz,debug,foo,help'
ARGS=$(getopt -s bash --options ${SHORTOPTS} --longoptions ${LONGOPTS} -- "$@")
eval set -- "${ARGS}" # see note regarding `getopt` and '--' below

# while true; do
## Wrong! above will cause the loop to always trigger default='*'.
## (Thanks ray_gun @ https://www.reddit.com/r/bash/comments/73n01j/bash_getopt_case_statement_fails_tests_needs_help/dnriuf6/ )
## instead loop only while there are options to parse
while [[ ${1} ]] ; do
    case ${1} in

        -h|--help)
            HELP=true
            # clear all other option vars
            BAR=false
            CAZ=false
            DEBUG=false
            FOO=false
            break
            ;;

        -b|--bar)
            BAR=true
            shift
            ;;

        -c|--caz)
            CAZ=true
            FOO=true
            BAR=true
            shift
            ;;

        -d|--debug)
            DEBUG=true
            shift
            ;;

        -f|--foo)
            FOO=true
            shift
            ;;

        ## thanks again to ray_gun @ https://www.reddit.com/r/bash/comments/73n01j/bash_getopt_case_statement_fails_tests_needs_help/dnriuf6/
        --) # `getopt` appends '--' to ${ARGS}, so gotta handle that
            shift
            break
            ;;

        *)
            HELP=true
            # clear all other option vars
            BAR=false
            CAZ=false
            DEBUG=false
            FOO=false
            break
            ;;
    esac
#    shift # NOOOO! will discard 2nd argument/option if present!
done

## -----------------------------------------------------------------------
## declare which options were set
## -----------------------------------------------------------------------

echo "${MESSAGE_PREFIX} set option vars="
# echo them in alpha order for testing
if ${BAR} ;   then echo 'BAR'   ; fi
if ${CAZ} ;   then echo 'CAZ'   ; fi
if ${DEBUG} ; then echo 'DEBUG' ; fi
if ${FOO} ;   then echo 'FOO'   ; fi
if ${HELP} ;  then echo 'HELP'  ; fi
